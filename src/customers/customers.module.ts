import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersController } from './customers.controller';
import { Customer } from './entities/customer.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Customer])], // เพิ่ม
  controllers: [CustomersController],
  providers: [CustomersService],
})
export class CustomersModule {
  constructor(private dataSource: DataSource) {} // เพิ่ม
}
