import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { OrderItem } from './entities/order-item';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private OrderRepository: Repository<Order>,
    @InjectRepository(Customer)
    private CustomerRepository: Repository<Customer>,
    @InjectRepository(Product)
    private ProductRepository: Repository<Product>,
    @InjectRepository(OrderItem)
    private OrderItemRepository: Repository<OrderItem>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    // save order
    const customer = await this.CustomerRepository.findOneBy({
      id: createOrderDto.customerId,
    });
    const order: Order = new Order();
    order.customer = customer;
    order.amount = 0;
    order.total = 0;
    await this.OrderRepository.save(order); // recieve id

    //loop for save orderItem and sum result

    for (const od of createOrderDto.orderItems) {
      const orderItem = new OrderItem();
      orderItem.amount = od.amount;
      orderItem.product = await this.ProductRepository.findOneBy({
        id: od.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.total = orderItem.price * orderItem.amount;
      orderItem.order = order; // อ้างกลับ

      await this.OrderItemRepository.save(orderItem);
      order.amount = orderItem.amount + order.amount;
      order.total = order.total + orderItem.total;
    }

    await this.OrderRepository.save(order); //recieve id
    return this.OrderRepository.findOne({
      where: { id: order.id },
      relations: ['orderItems'],
    }); //or return order
  }

  findAll() {
    return this.OrderRepository.find({ relations: ['customer', 'orderItems'] });
  }

  findOne(id: number) {
    return this.OrderRepository.findOne({
      where: { id: id },
      relations: ['customer', 'orderItems'],
    });
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const order = await this.OrderRepository.findOneBy({ id: id });
    return this.OrderRepository.softRemove(order);
  }
}
