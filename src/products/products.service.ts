import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private ProductRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto) {
    return this.ProductRepository.save(createProductDto);
  }

  findAll() {
    return this.ProductRepository.find();
  }

  async findOne(id: number) {
    const product = await this.ProductRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException();
    }
    return product;
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.ProductRepository.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException();
    }
    const updatedProduct = { ...product, ...updateProductDto };

    return this.ProductRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.ProductRepository.findOneBy({ id: id });
    if (!product) {
      throw new NotFoundException();
    }
    return this.ProductRepository.softRemove(product);
  }
}
